⚡️ `useModal`

useModal is a customizable React hook that simplifies the management of modals in your React applications. It provides a convenient way to handle modal visibility, open/close actions, and scroll locking.

⚡️ `Features`

Easily toggle the visibility of a modal with open, close, and toggle functions.
Option to lock the page scroll when the modal is open.
Customizable modal wrapper component with flexible styling options.
Smooth animation transitions for opening and closing the modal.
Ability to customize the overlay and handle click events outside the modal.
Lightweight and dependency-free.

⚡️ `Install`

Clone the app -> hit yarn, then -> yarn dev
-or using npm-
Clone the app -> hit npm install, then -> npm run dev

⚡️ `Usage`

I have put an example in App.tsx file.
