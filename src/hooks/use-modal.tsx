import { ElementType, FC, ReactNode, useCallback, useState } from 'react';

import classNames from 'classnames';

import { useLockPage } from './use-lock-page';

import './use-modal.css';

interface ModalWrapperProps {
  className?: string;
  isLockScroll?: boolean;
  hideOverlay?: boolean;
  children: ReactNode;
  onConfirm?: () => void;
}

export interface ModalResult {
  open: () => void;
  close: () => void;
  isOpen: boolean | undefined;
}

const useModal = (
  opened = false,
  isLockScroll = true
): [ElementType<ModalWrapperProps>, ModalResult] => {
  const [isOpen, setOpen] = useState<boolean | undefined>(opened || undefined);

  const { lockScroll, unlockScroll } = useLockPage();

  const open = useCallback(() => {
    if (isLockScroll) lockScroll();

    setOpen(true);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLockScroll]);

  const close = useCallback(() => {
    setOpen(false);

    if (isLockScroll) unlockScroll();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLockScroll]);

  const ModalWrapper: FC<ModalWrapperProps> = useCallback(
    ({ children, className = '', hideOverlay }) => {
      return (
        <div
          className={classNames('modal-wrapper', {
            open: isOpen,
          })}
        >
          {!hideOverlay && (
            <div
              className={classNames('overlay', {
                'open-overlay': isOpen,
                'close-overlay': !isOpen === false,
                'default-overlay': isOpen === undefined,
              })}
              onClick={close}
            ></div>
          )}
          <div className="modal-body">
            <div
              className={classNames(`modal-children ${className}`, {
                'open-modal': isOpen,
                'close-modal': isOpen === false,
                'default-closed': isOpen === undefined,
              })}
            >
              {children}
            </div>
          </div>
        </div>
      );
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isOpen]
  );

  return [ModalWrapper, { open, close, isOpen }];
};

export { useModal };
