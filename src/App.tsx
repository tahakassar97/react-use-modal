import { useModal } from './hooks';

function App() {
  const [Modal, { isOpen, close, open }] = useModal();

  const toggleModal = () => {
    if (isOpen) close();

    if (!isOpen) open();
  };

  return (
    <>
      <Modal>
        <>
          using Lorem Ipsum: Lorem ipsum dolor sit amet, consectetur adipiscing
          elit. Sed eleifend pretium dolor id congue. Nam sed sem justo. Sed
          mollis dolor at mi tincidunt commodo. Nulla facilisi. Sed pellentesque
          congue dolor, eu ultrices risus pulvinar sed. In hac habitasse platea
          dictumst. Nullam nec vestibulum risus. Mauris sollicitudin, enim non
          feugiat tincidunt, ligula ligula efficitur turpis, sed ullamcorper
          justo leo nec erat. Nullam pharetra, lorem eu condimentum efficitur,
          urna nisi suscipit purus, sed viverra lacus nunc sit amet purus. Nulla
          facilisi. Fusce vitae tellus tortor. Proin efficitur viverra ipsum, id
          facilisis arcu tincidunt id. Suspendisse potenti. Etiam suscipit justo
          augue, eget volutpat tortor gravida ac. Morbi mattis bibendum est, vel
          lobortis urna aliquet at. Maecenas vel neque arcu. Phasellus lobortis
          lacinia nisl id fringilla. Quisque eget ligula sem. Nulla facilisi.
          Phasellus in ipsum rhoncus, sodales felis eu, eleifend mi. Ut
          ullamcorper, elit nec fringilla placerat, odio ante gravida lacus, nec
          tempor urna neque at tortor. Vestibulum nec metus a ex consequat
          mollis id et ipsum. Nam id condimentum nisl. Cras iaculis aliquam
          orci, eget egestas felis eleifend in. Vestibulum semper sem id metus
          fringilla, sed posuere dui scelerisque. Aliquam ut mi ac nibh
          tristique tempor vitae sed nisl. Morbi ultricies consequat urna, eu
          laoreet sapien pharetra sed. Pellentesque habitant morbi tristique
          senectus et netus et malesuada fames ac turpis egestas. Fusce id
          sollicitudin lectus, at dignissim arcu. Sed ac quam rutrum,
          condimentum enim eu, rutrum mauris. Cras vulputate lobort
        </>
      </Modal>
      <main className="container">
        <button className="btn" onClick={toggleModal}>
          <span>{isOpen ? 'Close Modal' : 'Open Modal'}</span>
        </button>
      </main>
    </>
  );
}

export default App;
